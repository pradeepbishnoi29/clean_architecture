import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:storybook/resources/colors.dart';

class DLTStyles {
  static const TextStyle HEADLINE_BLACK = TextStyle(
      fontSize: 30,
      letterSpacing: 1.0,
      height: 1.11,
      fontFamily: 'ProximaNova-Medium',
      color: DltColors.black);
  static const TextStyle HEADLINE_RED = TextStyle(
      fontSize: 30,
      letterSpacing: 1.0,
      height: 1.11,
      fontFamily: 'ProximaNova-Medium',
      color: DltColors.red);
  static const TextStyle HEADLINE_1_RED = TextStyle(
      fontSize: 36,
      letterSpacing: 1.0,
      height: 1.11,
      fontFamily: 'ProximaNovaExCn-SemiboldIt',
      color: DltColors.red);

  static const TextStyle HEADLINE_1_BLACK = TextStyle(
      fontSize: 36,
      letterSpacing: 1.0,
      height: 1.11,
      fontFamily: 'ProximaNovaExCn-SemiboldIt',
      color: DltColors.black);

  static const TextStyle HEADLINE_1_BLACK_ITALIC = TextStyle(
      fontSize: 36,
      letterSpacing: 1.0,
      height: 1.11,
      fontStyle: FontStyle.italic,
      fontFamily: 'ProximaNovaExCn-SemiboldIt',
      color: DltColors.black);

  static const TextStyle HEADLINE_1_WHITE = TextStyle(
      fontSize: 36,
      letterSpacing: 1.0,
      height: 1.11,
      fontStyle: FontStyle.italic,
      fontFamily: 'ProximaNovaExCn-SemiboldIt',
      color: DltColors.white);

  static const TextStyle HEADLINE_1_WHITE_ITALIC = TextStyle(
      fontSize: 36,
      letterSpacing: 1.0,
      height: 1.11,
      fontStyle: FontStyle.italic,
      fontFamily: 'ProximaNovaExCn-SemiboldIt',
      color: DltColors.white);

  static const TextStyle HEADLINE_2 = TextStyle(
      fontSize: 23,
      letterSpacing: 0.5,
      height: 1.13,
      fontWeight: FontWeight.w600,
      fontStyle: FontStyle.normal,
      fontFamily: 'ProximaNovaCond-Semibold',
      color: DltColors.black);

  static const TextStyle HEADLINE_3 = TextStyle(
      fontSize: 20,
      letterSpacing: 0.5,
      height: 1.13,
      fontWeight: FontWeight.w600,
      fontStyle: FontStyle.normal,
      fontFamily: 'ProximaNovaCond-Semibold',
      color: DltColors.black);

  static const TextStyle HEADLINE_2_WHITE = TextStyle(
      fontSize: 23,
      letterSpacing: 0.5,
      height: 1.13,
      fontWeight: FontWeight.w600,
      fontStyle: FontStyle.normal,
      fontFamily: 'ProximaNovaCond-Semibold',
      color: DltColors.white);

  static const TextStyle HEADLINE_2_RED = TextStyle(
      fontSize: 23,
      letterSpacing: 0.5,
      height: 1.13,
      fontWeight: FontWeight.w600,
      fontStyle: FontStyle.normal,
      fontFamily: 'ProximaNovaCond-Semibold',
      color: DltColors.red);

  static const TextStyle SUBTITLE = TextStyle(
      fontSize: 20,
      letterSpacing: 1.5,
      fontWeight: FontWeight.w600,
      fontStyle: FontStyle.normal,
      fontFamily: 'ProximaNovaExCn-Semibold',
      color: DltColors.black);

  static const TextStyle SUBTITLE_WHITE = TextStyle(
      fontSize: 20,
      letterSpacing: 1.5,
      fontWeight: FontWeight.w600,
      fontStyle: FontStyle.normal,
      fontFamily: 'ProximaNovaExCn-Semibold',
      color: DltColors.white);

  static const TextStyle CALL_TO_ACTION_BLACK = TextStyle(
      fontSize: 16,
      letterSpacing: 1.0,
      height: 1.25,
      fontFamily: 'ProximaNovaCond-Bold',
      color: DltColors.black);

  static const TextStyle CALL_TO_ACTION_RED = TextStyle(
      fontSize: 16,
      letterSpacing: 1.0,
      height: 1.25,
      fontFamily: 'ProximaNovaCond-Bold',
      color: DltColors.red);

  static const TextStyle CALL_TO_ACTION_WHITE = TextStyle(
      fontSize: 16,
      letterSpacing: 1.0,
      height: 1.25,
      fontFamily: 'ProximaNovaCond-Bold',
      color: DltColors.white);

  static const TextStyle CTA_INACTIVE = TextStyle(
      fontSize: 16,
      letterSpacing: 1.0,
      height: 1.25,
      fontFamily: 'ProximaNovaCond-Bold',
      color: DltColors.gray2);

  static const TextStyle FIELD_INPUT = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 18,
      letterSpacing: 0.25,
      height: 1.22,
      decoration: TextDecoration.none,
      fontFamily: 'ProximaNova-Semibold',
      color: DltColors.black);

  static const TextStyle CT_DISABLED = TextStyle(
      fontWeight: FontWeight.w600,
      fontSize: 18,
      letterSpacing: 0.25,
      height: 1.22,
      fontFamily: 'ProximaNova-Semibold',
      color: DltColors.hintText);

  static const TextStyle LABEL_BLACK = TextStyle(
      fontSize: 15,
      letterSpacing: 0.25,
      height: 1.27,
      fontWeight: FontWeight.w500,
      fontFamily: 'ProximaNova-Medium',
      color: DltColors.black);

  static const TextStyle LABEL_WHITE = TextStyle(
      fontSize: 15,
      letterSpacing: 0.25,
      height: 1.27,
      fontWeight: FontWeight.w500,
      fontFamily: 'ProximaNova-Medium',
      color: DltColors.white);

  static const TextStyle ERROR = TextStyle(
    fontSize: 15,
    letterSpacing: 0.25,
    height: 1.27,
    fontWeight: FontWeight.w600,
    fontFamily: 'ProximaNova-Semibold',
    color: DltColors.red,
  );

  static const TextStyle ACTION_LABEL_BLUE = TextStyle(
    fontSize: 15,
    letterSpacing: 0.25,
    height: 1.27,
    fontWeight: FontWeight.w600,
    fontFamily: 'ProximaNova-Semibold',
    color: DltColors.utility1,
  );

  static const TextStyle ACTION_LABEL_INACTIVE = TextStyle(
    fontSize: 15,
    letterSpacing: 0.25,
    height: 1.27,
    fontWeight: FontWeight.w600,
    fontFamily: 'ProximaNova-Semibold',
    color: DltColors.gray2,
  );

  static const TextStyle ACTION_LABEL_BLACK = TextStyle(
    fontSize: 15,
    letterSpacing: 0.25,
    height: 1.27,
    fontWeight: FontWeight.w600,
    fontFamily: 'ProximaNova-Semibold',
    color: DltColors.black,
  );

  static const TextStyle ACTION_LABEL_GREEN = TextStyle(
    fontSize: 15,
    letterSpacing: 0.25,
    height: 1.27,
    fontWeight: FontWeight.w600,
    fontFamily: 'ProximaNova-Semibold',
    color: DltColors.utility2,
  );

  static const TextStyle ACTION_LABEL_BLACK_BOLD = TextStyle(
    fontSize: 15,
    letterSpacing: 0.25,
    height: 1.27,
    fontWeight: FontWeight.w700,
    fontFamily: 'ProximaNova-Semibold',
    color: DltColors.black,
  );

  static const TextStyle ACTION_LABEL_WHITE = TextStyle(
    fontSize: 15,
    letterSpacing: 0.25,
    height: 1.27,
    fontWeight: FontWeight.w600,
    fontFamily: 'ProximaNova-Semibold',
    color: DltColors.white,
  );

  static const TextStyle STRIKE_THROUGH_LABEL = TextStyle(
    fontSize: 15,
    letterSpacing: 0.25,
    height: 1.27,
    fontWeight: FontWeight.w600,
    fontFamily: 'ProximaNova-Semibold',
    decoration: TextDecoration.lineThrough,
    color: DltColors.gray2,
  );

  static const TextStyle BODY_COPY_ACTION = TextStyle(
    fontSize: 17,
    letterSpacing: 0.25,
    height: 1.41,
    fontWeight: FontWeight.w500,
    fontFamily: 'ProximaNova-Medium',
    color: DltColors.black,
  );

  static TextStyle caption = _captionFactory();
  static TextStyle captionRed = _captionFactory(color: DltColors.red);
  static TextStyle captionBlue =
      _captionFactory(color: DltColors.accentColorBlue);
  static TextStyle captionGreen = _captionFactory(color: DltColors.utility2);
  static TextStyle captionGray = _captionFactory(color: DltColors.gray2);
  static TextStyle promoTextGreen = _captionFactory(color: DltColors.promoTextGreen);

  static TextStyle _captionFactory({Color color = DltColors.black}) {
    return TextStyle(
      fontSize: 13,
      letterSpacing: 0.25,
      height: 1.23,
      fontWeight: FontWeight.w500,
      fontFamily: 'ProximaNova-Medium',
      color: color,
    );
  }

  static const TextStyle NAV_LABEL = TextStyle(
      fontSize: 11,
      letterSpacing: 0.2,
      height: 1.18,
      fontWeight: FontWeight.w600,
      fontFamily: 'ProximaNova-Semibold',
      color: DltColors.black);

  static const TextStyle NAV_INACTIVE = TextStyle(
    fontSize: 11,
    letterSpacing: 0.2,
    height: 1.18,
    fontWeight: FontWeight.w600,
    fontFamily: 'ProximaNova-Semibold',
    color: DltColors.gray2,
  );

  static TextStyle navLabelWith(Color color) {
    return TextStyle(
      fontSize: 11,
      letterSpacing: 0.2,
      height: 1.18,
      fontWeight: FontWeight.w600,
      fontFamily: 'ProximaNova-Semibold',
      color: color,
    );
  }

  static const TextStyle HEADLINE_2_WHITE_ITALIC = TextStyle(
    fontSize: 24,
    letterSpacing: 1.0,
    height: 1.5,
    fontFamily: 'ProximaNovaExCn-BoldIt',
    color: DltColors.white,
  );
}
