import 'package:flutter/material.dart';

/// Opacity Conversions:
/// 100% - FF
/// 90%  - E6
/// 80%  - CC
/// 70%  - B3
/// 60%  - 99
/// 50%  - 80
/// 40%  - 66
/// 30%  - 4D
/// 20%  - 33
/// 10%  - 1A
/// 0%   - 00

class DltColors {
  static const MaterialColor black = const MaterialColor(
    0xFF000000,
    const <int, Color>{
      50: const Color(0x1A000000),
      100: const Color(0x33000000),
      200: const Color(0x4D000000),
      300: const Color(0x66000000),
      400: const Color(0x80000000),
      500: const Color(0x99000000),
      600: const Color(0xB3000000),
      700: const Color(0xCC000000),
      800: const Color(0xE6000000),
      900: const Color(0xFF000000),
    },
  );

  static const MaterialColor red = const MaterialColor(
    0xFFE60D2E,
    const <int, Color>{
      50: const Color(0x1AE60D2E),
      100: const Color(0x33E60D2E),
      200: const Color(0x4DE60D2E),
      300: const Color(0x66E60D2E),
      400: const Color(0x80E60D2E),
      500: const Color(0x99E60D2E),
      600: const Color(0xB3E60D2E),
      700: const Color(0xCCE60D2E),
      800: const Color(0xE6E60D2E),
      900: const Color(0xFFE60D2E),
    },
  );

  static const MaterialColor white = const MaterialColor(
    0xFFFFFFFF,
    const <int, Color>{
      50: const Color(0x1AFFFFFF),
      100: const Color(0x33FFFFFF),
      200: const Color(0x4DFFFFFF),
      300: const Color(0x66FFFFFF),
      400: const Color(0x80FFFFFF),
      500: const Color(0x99FFFFFF),
      600: const Color(0xB3FFFFFF),
      700: const Color(0xCCFFFFFF),
      800: const Color(0xE6FFFFFF),
      900: const Color(0xFFFFFFFF),
    },
  );

  static const MaterialColor gray1 = const MaterialColor(
    0xFF4D4D4D,
    const <int, Color>{
      50: const Color(0x1A4D4D4D),
      100: const Color(0x334D4D4D),
      200: const Color(0x4D4D4D4D),
      300: const Color(0x664D4D4D),
      400: const Color(0x804D4D4D),
      500: const Color(0x994D4D4D),
      600: const Color(0xB34D4D4D),
      700: const Color(0xCC4D4D4D),
      800: const Color(0xE64D4D4D),
      900: const Color(0xFF4D4D4D),
    },
  );

  static const MaterialColor gray2 = const MaterialColor(
    0xFF929292,
    const <int, Color>{
      50: const Color(0x1A929292),
      100: const Color(0x33929292),
      200: const Color(0x4D929292),
      300: const Color(0x66929292),
      400: const Color(0x80929292),
      500: const Color(0x99929292),
      600: const Color(0xB3929292),
      700: const Color(0xCC929292),
      800: const Color(0xE6929292),
      900: const Color(0xFF929292),
    },
  );

  static const MaterialColor gray3 = const MaterialColor(
    0xFFEAEAEA,
    const <int, Color>{
      50: const Color(0x1AEAEAEA),
      100: const Color(0x33EAEAEA),
      200: const Color(0x4DEAEAEA),
      300: const Color(0x66EAEAEA),
      400: const Color(0x80EAEAEA),
      500: const Color(0x99EAEAEA),
      600: const Color(0xB3EAEAEA),
      700: const Color(0xCCEAEAEA),
      800: const Color(0xE6EAEAEA),
      900: const Color(0xFFEAEAEA),
    },
  );

  static const MaterialColor gray4 = const MaterialColor(
    0xFFF5F5F5,
    const <int, Color>{
      50: const Color(0x1AF5F5F5),
      100: const Color(0x33F5F5F5),
      200: const Color(0x4DF5F5F5),
      300: const Color(0x66F5F5F5),
      400: const Color(0x80F5F5F5),
      500: const Color(0x99F5F5F5),
      600: const Color(0xB3F5F5F5),
      700: const Color(0xCCF5F5F5),
      800: const Color(0xE6F5F5F5),
      900: const Color(0xFFF5F5F5),
    },
  );

  static const MaterialColor gray5 = const MaterialColor(
    0xFFF3F3F3,
    const <int, Color>{
      50: const Color(0x1AF3F3F3),
      100: const Color(0x33F3F3F3),
      200: const Color(0x4DF3F3F3),
      300: const Color(0x66F3F3F3),
      400: const Color(0x80F3F3F3),
      500: const Color(0x99F3F3F3),
      600: const Color(0xB3F3F3F3),
      700: const Color(0xCCF3F3F3),
      800: const Color(0xE6F3F3F3),
      900: const Color(0xFFF3F3F3),
      1000: const Color(0x1AF6F6F6),
    },
  );

  static const MaterialColor accent1 = const MaterialColor(
    0xFFad9966,
    const <int, Color>{
      50: const Color(0x1Aad9966),
      100: const Color(0x33ad9966),
      200: const Color(0x4Dad9966),
      300: const Color(0x66ad9966),
      400: const Color(0x80ad9966),
      500: const Color(0x99ad9966),
      600: const Color(0xB3ad9966),
      700: const Color(0xCCad9966),
      800: const Color(0xE6ad9966),
      900: const Color(0xFFad9966),
    },
  );

  static const MaterialColor utility1 = const MaterialColor(
    0xFF2e72b0,
    const <int, Color>{
      50: const Color(0x1A2e72b0),
      100: const Color(0x332e72b0),
      200: const Color(0x4D2e72b0),
      300: const Color(0x662e72b0),
      400: const Color(0x802e72b0),
      500: const Color(0x992e72b0),
      600: const Color(0xB32e72b0),
      700: const Color(0xCC2e72b0),
      800: const Color(0xE62e72b0),
      900: const Color(0xFF2e72b0),
    },
  );

  static const MaterialColor utility2 = const MaterialColor(
    0xFF578225,
    const <int, Color>{
      50: const Color(0x1A578225),
      100: const Color(0x33578225),
      200: const Color(0x4D578225),
      300: const Color(0x66578225),
      400: const Color(0x80578225),
      500: const Color(0x99578225),
      600: const Color(0xB3578225),
      700: const Color(0xCC578225),
      800: const Color(0xE6578225),
      900: const Color(0xFF578225),
    },
  );

  static const Color clear = const Color(0x00FFFFFF);

  // accent colors - accent colors provide contrast with primary colors to highlight UI components
  static const Color accentColorBlue = Color(0xFF007CB5);
  static Color accentColorGreen = Color(0xFF008100);
  static Color accentColorYellow = Color(0xFFF7B500);
  static Color accentColorBrown = Color(0xFFB0995E);

  // shop header carousel promotional text green
  static Color promoTextGreen = Color(0xFF008100);

  // disabled button
  static Color disabledButton = Colors.grey[200];

  // cta disabled button
  static Color ctaDisabledButton = Color(0x1AF5F5F5);

  // hint text
  static const Color hintText = Color(0x99929292);

  // close button
  static Color closeButton = Color(0xFFA1A1A1);

  static Color gradientStart = Color(0xFFE9F0F2);
  static Color gradientEnd = Color(0xFFF5F6F7);
}
