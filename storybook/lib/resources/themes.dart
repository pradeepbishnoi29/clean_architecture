import 'package:flutter/material.dart';
import 'package:storybook/resources/colors.dart';
import 'package:storybook/resources/styles.dart';

var dltTheme = ThemeData(
  primarySwatch: DltColors.white,
);

class DLTTextFormFieldTheme {
  InputDecorationTheme inputDecorationTheme = InputDecorationTheme(
      labelStyle: DLTStyles.LABEL_BLACK,
      errorStyle: DLTStyles.ERROR,
      hintStyle: DLTStyles.CT_DISABLED,
      errorMaxLines: 4,
      helperStyle: DLTStyles.LABEL_BLACK,
      contentPadding: EdgeInsets.only(bottom: 17 , left: 10, top: 4)
  );


  Color cursorColor = DltColors.red;
  Color backgroundColor = DltColors.gray4;
  Color unfocusOutlineColor = DltColors.gray2;
  Color focusOutlineColor = DltColors.black;
  Color iconColor = DltColors.gray1;
  Color errorColor = DltColors.red;
  TextStyle primaryTextStyle = DLTStyles.FIELD_INPUT;
  TextStyle optionalStyle = DLTStyles.NAV_INACTIVE;
  TextStyle optionalTitle =  DLTStyles.NAV_LABEL;
}

class DLTPasswordRequirementsTheme {
  Color passColor = DltColors.utility2;
  Color errorColor = DltColors.red;
  TextStyle primaryTextStyle = DLTStyles.LABEL_BLACK;
}