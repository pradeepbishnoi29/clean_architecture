import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:storybook/widgets/utils/keys.dart';

class DltIcons {

  static Icon starFull({Color color, double size = 28, Key key}) {
    return Icon(Icons.star, key: key, color: color, size: size);
  }

  static Icon starHalf({Color color, double size = 28, Key key}) {
    return Icon(Icons.star_half, key: key, color: color, size: size);
  }

  static Widget errorAlert({double size = 28, Key key}) {
    return SvgPicture.asset('storybook/assets/icons/icon_error_alert.svg', width: size, height: size);
  }
  static Icon starBorder({Color color, double size = 28, Key key}) {
    return Icon(Icons.star_border, key: key, color: color, size: size);
  }

  static Widget successAlert({double size = 28, Key key}) {
    return SvgPicture.asset('storybook/assets/icons/icon_success_alert.svg', width: size, height: size);
  }
  static Widget warningAlert({double size = 28, Key key}) {
    return SvgPicture.asset('storybook/assets/icons/icon_warning_alert.svg', width: size, height: size);
  }

  // regular use
  static Icon clear({Color color}) {
    return Icon(Icons.highlight_off, key: Key("clear icon"), color: color);
  }

  static Icon checkCircle(Color color, {double size = 28}) {
    return Icon(Icons.check_circle, key: Key("check circle icon"), color: color, size: size);
  }

  static Icon check(Color color, {double size = 20}) {
    return Icon(Icons.check, key: Key("check icon"), color: color, size: size);
  }

  static Icon error({Color color, double size = 24}) {
    return Icon(Icons.error, key: Key("error icon"), color: color, size: size);
  }

  static Icon visibility({Color color}) {
    return Icon(Icons.visibility, key: Key("show field icon"), color: color);
  }

  static Icon visibilityOff({Color color}) {
    return Icon(Icons.visibility_off, key: Key("hide field icon"), color: color);
  }

  static Icon close({Color color}) {
    return Icon(Icons.close, key: Key(StorybookKeys.closeIcon), color: color);
  }

  static Widget _iconAssetWith(String assetString, Color color, double width, double height, {Key key}){
    Color _color = (color == null)? Colors.black : color;

    return SvgPicture.asset(assetString, color: _color, fit: BoxFit.contain, width: width, height: height, key: key);
  }
}