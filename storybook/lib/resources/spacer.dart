import 'package:flutter/cupertino.dart';

const SPACER_4 = 4.0;
const SPACER_8 = 8.0;
const SPACER_16 = 16.0;
const SPACER_24 = 24.0;
const SPACER_32 = 32.0;
const SPACER_40 = 40.0;
const SPACER_48 = 48.0;
const SPACER_56 = 56.0;
const SPACER_64 = 64.0;
const SPACER_72 = 72.0;
const SPACER_80 = 80.0;
const SPACER_88 = 88.0;
const SPACER_96 = 96.0;
const SPACER_104 = 104.0;
const SPACER_112 = 112.0;
const SPACER_136 = 136.0;

Widget generateSpacer(double amount) {
  return SizedBox(width: amount, height: amount);
}