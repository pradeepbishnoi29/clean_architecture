import 'package:flutter/material.dart';
import 'package:storybook/widgets/textformfields/helpers/text_form_field_property_bundle.dart';
import 'package:storybook/widgets/textformfields/helpers/text_form_field_type_enumeration.dart';
import 'package:storybook/widgets/textformfields/validators/email_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/string_validator.dart';
import 'package:storybook/widgets/textformfields/validators/required_field_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/non_required_field_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validating_textformfield.dart';
import 'package:storybook/widgets/textformfields/validators/includes_1_lowercase_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/includes_1_number_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/includes_1_uppercase_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/min_8_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/password_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/zip_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/mobile_validator_impl.dart';
import 'package:storybook/resources/themes.dart';

class ValidatingTextFormFieldFactory {
  static Widget fromStyle(
      {Key key, TextFormFieldTypeEnumeration textFormFieldType, TextFormFieldPropertyBundle textformfieldbundle}) {
    TextFormFieldPropertyBundle _bundle = (textformfieldbundle ??
        TextFormFieldPropertyBundle());
    if (_bundle.theme == null) {
      _bundle.theme = DLTTextFormFieldTheme();
    }

    switch (textFormFieldType) {
      case TextFormFieldTypeEnumeration.EMAIL:
        EmailValidatorImpl validation = EmailValidatorImpl(_bundle.fieldError);
        _bundle.keyboardType = TextInputType.emailAddress;
        _bundle.validator = validation;
        _bundle.label ??= "Email";
        break;

      case TextFormFieldTypeEnumeration.PASSWORD:
        PasswordValidatorImpl validation = PasswordValidatorImpl(
            _bundle.fieldError);
        List<StringValidator> _passwordReqInstantiations = [
          Min8ValidatorImpl(),
          Includes1UppercaseValidatorImpl(),
          Includes1LowercaseValidatorImpl(),
          Includes1NumberValidatorImpl()
        ];
        validation.validators = _passwordReqInstantiations;
        _bundle.validator = validation;
        _bundle.label ??= "Password";
        _bundle.isAggregateValidator = true;
        break;

      case TextFormFieldTypeEnumeration.REQUIRED:
        RequiredFieldValidatorImpl validation = RequiredFieldValidatorImpl(
            _bundle.fieldError);
        _bundle.validator = validation;
        break;

      case TextFormFieldTypeEnumeration.NONREQUIRED:
        NonRequiredFieldValidatorImpl validation = NonRequiredFieldValidatorImpl(
            _bundle.fieldError);
        _bundle.validator = validation;
        break;

      case TextFormFieldTypeEnumeration.ZIP:
        ZipValidatorImpl validation = ZipValidatorImpl(_bundle.fieldError);
        _bundle.validator = validation;
        break;

      case TextFormFieldTypeEnumeration.MOBILE:
        MobileValidatorImpl validation = MobileValidatorImpl(_bundle.fieldError);
        _bundle.validator = validation;
        break;

      default:
        break;
    }

    return ValidatingTextFormField(key: key, bundle: _bundle);
  }
}
