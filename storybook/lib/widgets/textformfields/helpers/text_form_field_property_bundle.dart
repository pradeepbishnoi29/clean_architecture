import 'package:flutter/material.dart';
import 'package:storybook/widgets/textformfields/validators/string_validator.dart';
import 'package:storybook/resources/themes.dart';

class TextFormFieldPropertyBundle {
  TextInputType keyboardType = TextInputType.text;
  FocusNode focusNode;
  DLTTextFormFieldTheme theme;
  TextEditingController controller;
  StringValidator validator;
  String label;
  String helperText;
  String optionalString;
  String fieldError;
  bool obscureText;
  Function onSaved;
  Function onSubmitted;
  Function onChanged;
  bool isAggregateValidator;
  bool enabled;
  IconButton suffixIcon;
  String mobileCode;
  int maxCharactersLength;

  TextFormFieldPropertyBundle(
      {FocusNode focusNode, this.keyboardType, this.theme, this.controller, this.validator, this.label, this.helperText, this.optionalString, this.mobileCode, this.maxCharactersLength,
        this.fieldError, this.enabled = true, this.obscureText = false, this.onSaved, this.onSubmitted, this.onChanged, this.isAggregateValidator = false, this.suffixIcon})
      : this.focusNode = focusNode ?? FocusNode();
}