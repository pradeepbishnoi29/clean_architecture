import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:storybook/resources/colors.dart';
import 'package:storybook/widgets/textformfields/helpers/text_form_field_property_bundle.dart';
import 'package:storybook/widgets/textformfields/validators/aggregate_validator.dart';
import 'package:storybook/resources/spacer.dart';
import 'package:storybook/resources/themes.dart';
import 'package:storybook/resources/icons.dart';
import 'package:storybook/widgets/textformfields/validators/field_error_validator.dart';
import 'package:storybook/widgets/textformfields/validators/string_validator.dart';
import 'package:storybook/widgets/utils/keys.dart';

class ValidatingTextFormField extends StatefulWidget {
  final TextFormFieldPropertyBundle bundle;

  ValidatingTextFormField({Key key, this.bundle}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ValidatingTextFormFieldState();
}

class _ValidatingTextFormFieldState extends State<ValidatingTextFormField> {
  _ValidatingTextFormFieldState();

  final _formFieldKey = GlobalKey<FormFieldState>();

  TextFormFieldPropertyBundle bundle;
  bool _obscure = false;
  bool _isAggregateValidator = false;
  AggregateValidator _aggValidator;
  TextEditingController _controller;
  DLTTextFormFieldTheme _theme;
  Function _onChanged;
  Function _onSubmitted;
  String mobileCode;

  @override
  void initState() {
    super.initState();
    bundle = widget.bundle ?? TextFormFieldPropertyBundle();
    _obscure = bundle.obscureText;
    _theme = (bundle.theme != null) ? bundle.theme : DLTTextFormFieldTheme();
    if (bundle.isAggregateValidator) {
      _isAggregateValidator = true;
      _aggValidator = bundle.validator as AggregateValidator;
    }
    _controller = (bundle.controller != null)
        ? bundle.controller
        : new TextEditingController();
    bundle.focusNode.addListener(_handleFocusOrTextChange);
    _controller.addListener(_handleFocusOrTextChange);
    mobileCode = bundle.mobileCode;
    _onChanged = (str) {
      _formFieldKey.currentState.validate();
      if (bundle.onChanged != null) {
        bundle.onChanged(str);
      }
    };

    _onSubmitted = (str) {
      _formFieldKey.currentState.validate();
      if (bundle.onSubmitted != null) {
        bundle.onSubmitted(str);
      }
    };
  }

  @override
  void didUpdateWidget(ValidatingTextFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.bundle != oldWidget.bundle) {
      bundle.keyboardType = widget.bundle.keyboardType;
      bundle.maxCharactersLength = widget.bundle.maxCharactersLength;
      mobileCode = widget.bundle.mobileCode;
      bundle.label = widget.bundle.label;
      setState(() {

      });
    }
  }

  @override
  void dispose() {
    bundle.focusNode.removeListener(_handleFocusOrTextChange);
    _controller.removeListener(_handleFocusOrTextChange);

    super.dispose();
  }

  void _handleFocusOrTextChange() {
    setState(() {
      if (!bundle.focusNode.hasFocus && bundle.obscureText) {
        _obscure = true;
      }
      var validator = bundle.validator;
      if (validator is FieldErrorValidatorImpl) {
        validator.resetFieldError();
      }
    });
  }

  void _toggleObscurity() {
    setState(() {
      _obscure = !_obscure;
    });
  }

  bool _hasError(){
    var currentState = _formFieldKey.currentState;
    if (currentState == null){
      return false;
    }
    return currentState.errorText != null && currentState.errorText.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    if ((bundle.fieldError ?? "").isNotEmpty && !_hasError()) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _formFieldKey.currentState.validate();
      });
    }
    return Container(
      padding: EdgeInsets.zero,
      child: Column(children: <Widget>[
        _textField(),
        (_isAggregateValidator)
            ? Padding(
                padding: const EdgeInsets.only(top: SPACER_16),
                child:
                    _aggValidator.validationRequirementsView(_controller.text))
            : Container()
      ]),
    );
  }

  Widget _textField() {
    return TextFormField(
      enabled: bundle.enabled,
      key: _formFieldKey,
      focusNode: bundle.focusNode,
      controller: _controller,
      validator: (bundle.validator == null) ? null : bundle.validator.validator,
      onSaved: bundle.onSaved,
      onChanged: _onChanged,
      onFieldSubmitted: _onSubmitted,
      autovalidate: false,
      obscureText: _obscure,
      cursorColor: _theme.cursorColor,
      style: _theme.primaryTextStyle,
      keyboardType: bundle.keyboardType,
      inputFormatters: bundle.maxCharactersLength == null ||
              bundle.maxCharactersLength <= 0
          ? []
          : [
              new LengthLimitingTextInputFormatter(bundle.maxCharactersLength),
            ],
      decoration: InputDecoration(
          hintText: bundle.focusNode.hasFocus ? null : bundle.label,
          hintStyle: _theme.inputDecorationTheme.hintStyle,
          labelText: bundle.label,
          labelStyle: (_controller.text.isEmpty && bundle.focusNode.hasFocus == false) ? _theme.inputDecorationTheme.hintStyle : _theme.inputDecorationTheme.labelStyle,
          prefixStyle: _theme.primaryTextStyle,
          errorStyle: _theme.inputDecorationTheme.errorStyle,
          errorMaxLines: _theme.inputDecorationTheme.errorMaxLines,
          helperText: bundle.helperText,
          helperStyle: _theme.inputDecorationTheme.helperStyle,
          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: _theme.unfocusOutlineColor, width: 1),),
          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: _theme.focusOutlineColor, width: 2),),
          filled: true,
          alignLabelWithHint: true,
          fillColor: _theme.backgroundColor,
          suffixIcon: _suffixIcon(),
          prefixText: bundle.mobileCode != null && bundle.mobileCode.isNotEmpty
              ? mobileCode + ' | '
              : null,
          contentPadding: _controller.text.isNotEmpty ||  bundle.focusNode.hasFocus ? EdgeInsets.only(bottom: 7 , left: 10, top: 4) : _theme.inputDecorationTheme.contentPadding),
    );
  }

  Widget _suffixIcon() {
    if (bundle.focusNode.hasFocus) {
      if (_obscure) {
        return IconButton(
          key: Key(StorybookKeys.obscurityToggleButton),
          color: _theme.iconColor,
          splashColor: DltColors.clear,
          highlightColor: DltColors.clear,
          icon: (_obscure) ? DltIcons.visibility() : DltIcons.visibilityOff(),
          onPressed: _toggleObscurity,
        );
      } else {
        if (_controller.text.isEmpty) {
          return null;
        } else {
          return IconButton(
            key: Key(StorybookKeys.clearFormFieldButton),
            color: _theme.iconColor,
            splashColor: DltColors.clear,
            highlightColor: DltColors.clear,
            icon: DltIcons.clear(),
            onPressed: () {
              Future.delayed(Duration(milliseconds: 50)).then((_) {
                _controller.clear();
                _onChanged(_controller.text);
              });
            },
          );
        }
      }
    } else {
      if (_hasError()) {
        return DltIcons.error(color: _theme.errorColor);
      } else {
        if (_obscure) {
          return DltIcons.visibility(color: _theme.iconColor);
        } else {
          return (bundle.suffixIcon == null) ? null : bundle.suffixIcon;
        }
      }
    }
  }
}

//Validatingtextformfield implementations
class ValidatingTextFormFieldBasic extends ValidatingTextFormField {
  ValidatingTextFormFieldBasic()
      : super(
            bundle: TextFormFieldPropertyBundle(
                enabled: false,
                label: "Label",
                helperText: "Helper Text",
                controller: TextEditingController(text: " ")));
}

class ValidatingTextFormFieldUnfocused extends ValidatingTextFormField {
  ValidatingTextFormFieldUnfocused()
      : super(
            bundle: TextFormFieldPropertyBundle(
                label: "Label",
                helperText: "Helper Text",
                controller: TextEditingController(text: " ")));
}

class ValidatingTextFormFieldFilled extends ValidatingTextFormField {
  ValidatingTextFormFieldFilled({String fieldText})
      : super(
            bundle: TextFormFieldPropertyBundle(
                label: "Label",
                helperText: "Helper Text",
                controller: TextEditingController(text: fieldText)));
}

class ValidatingTextFormFieldFilledError extends ValidatingTextFormField {
  ValidatingTextFormFieldFilledError(
      {String fieldText, StringValidator validator})
      : super(
            bundle: TextFormFieldPropertyBundle(
                label: "Label",
                helperText: "Helper Text",
                validator: validator,
                controller: TextEditingController(text: fieldText)));
}

class ValidatingTextFormFieldObscured extends ValidatingTextFormField {
  ValidatingTextFormFieldObscured({String fieldText})
      : super(
            bundle: TextFormFieldPropertyBundle(
                label: "Label",
                helperText: "Helper Text",
                controller: TextEditingController(text: fieldText),
                obscureText: true));
}
