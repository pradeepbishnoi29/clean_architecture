import 'package:flutter/cupertino.dart';
import 'string_validator.dart';

class Includes1NumberValidatorImpl extends StringValidator {
  static const String DESCRIPTION = "Includes at least 1 number";
  static const String REGEX = r'(?=.*[0-9])';

  String get description => DESCRIPTION;
  
  FormFieldValidator<String> get validator {
    return (String value) {
      bool passValid = RegExp('^'+REGEX).hasMatch(value);
      if (value.isEmpty || !passValid) {
        return DESCRIPTION;
      } else {
        return null;
      }
    };
  }
}