import 'package:flutter/cupertino.dart';
import 'package:storybook/widgets/textformfields/validators/field_error_validator.dart';

class RequiredFieldValidatorImpl extends FieldErrorValidatorImpl {
  static const String DESCRIPTION = "Field Required";

  RequiredFieldValidatorImpl([String fieldErrorDescription]) : super(fieldErrorDescription);

  String get description => DESCRIPTION;
  
  FormFieldValidator<String> get validator {
    return (String value) {
      if (value.isEmpty) {
        return DESCRIPTION;
      } else {
        return fieldErrorDescription;
      }
    };
  }
}