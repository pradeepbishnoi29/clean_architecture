import 'package:flutter/cupertino.dart';
import 'package:storybook/widgets/textformfields/validators/field_error_validator.dart';

class EmailValidatorImpl extends FieldErrorValidatorImpl {
  static const String EMAIL_REQUIRED = "Email Required";
  static const String EMAIL_INVALID = "Email Invalid";
  static const String REGEX = r"[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+\.[a-zA-Z]+";

  EmailValidatorImpl([String fieldErrorDescription]) : super(fieldErrorDescription);

  String get description => EMAIL_INVALID;

  FormFieldValidator<String> get validator {
    return (String value) {
      bool emailValid = RegExp('^'+REGEX).hasMatch(value);
      if (value.isEmpty) {
        return EMAIL_REQUIRED;
      } else if (!emailValid) {
        return EMAIL_INVALID;
      } else {
        return fieldErrorDescription;
      }
    };
  }
}