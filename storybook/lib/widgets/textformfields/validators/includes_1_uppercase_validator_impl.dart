import 'package:flutter/cupertino.dart';
import 'string_validator.dart';

class Includes1UppercaseValidatorImpl extends StringValidator {
  static const String DESCRIPTION = "Includes at least 1 uppercase letter";
  static const String REGEX = r'(?=.*[A-Z])';

  String get description => DESCRIPTION;

  FormFieldValidator<String> get validator {
    return (String value) {
      bool passValid = RegExp('^'+REGEX).hasMatch(value);
      if (value.isEmpty || !passValid) {
        return DESCRIPTION;
      } else {
        return null;
      }
    };
  }
}