import 'package:flutter/cupertino.dart';
import 'package:storybook/widgets/textformfields/validators/string_validator.dart';

abstract class AggregateValidator {
  List<StringValidator> validators;
  List<String> validationResults(String value);
  Widget validationRequirementsView(String value);
}