import 'string_validator.dart';

abstract class FieldErrorValidatorImpl extends StringValidator {
  String fieldErrorDescription;
  FieldErrorValidatorImpl(this.fieldErrorDescription);
  void resetFieldError() {
    fieldErrorDescription = null;
  }
}