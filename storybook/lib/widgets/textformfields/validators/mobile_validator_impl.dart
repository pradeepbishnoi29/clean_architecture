import 'package:flutter/cupertino.dart';
import 'package:storybook/widgets/textformfields/validators/field_error_validator.dart';

class MobileValidatorImpl extends FieldErrorValidatorImpl {
  static const String MOBILE_REQUIRED = "Phone number is Required";
  static const String MOBILE_INVALID = "Phone Invalid";
  static const String REGEX = r"\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$";

  MobileValidatorImpl([String fieldErrorDescription])
      : super(fieldErrorDescription);

  String get description => MOBILE_REQUIRED;

  FormFieldValidator<String> get validator {
    return (String value) {
      if (value.isEmpty) {
        return MOBILE_REQUIRED;
      }
      else {
        return fieldErrorDescription;
      }
    };
  }
}