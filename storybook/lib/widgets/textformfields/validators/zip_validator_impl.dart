import 'package:flutter/cupertino.dart';
import 'package:storybook/widgets/textformfields/validators/field_error_validator.dart';

class ZipValidatorImpl extends FieldErrorValidatorImpl {
  static const String ZIP_REQUIRED = "Zip or Postal Code is required";
  static const String ZIP_INVALID = "Zip Invalid";
  static const String REGEX = r"\d{5}(?:[-\s]\d{4})?$";

  ZipValidatorImpl([String fieldErrorDescription])
      : super(fieldErrorDescription);

  String get description => ZIP_REQUIRED;

  FormFieldValidator<String> get validator {
    return (String value) {
      if (value.isEmpty) {
        return ZIP_REQUIRED;
      }
      else {
        return fieldErrorDescription;
      }
    };
  }
}
