import 'package:flutter/cupertino.dart';
import 'string_validator.dart';

class Includes1LowercaseValidatorImpl extends StringValidator {
  static const String DESCRIPTION = "Includes at least 1 lowercase letter";
  static const String REGEX = r'(?=.*[a-z])';

  String get description => DESCRIPTION;

  FormFieldValidator<String> get validator {
    return (String value) {
      bool passValid = RegExp('^'+REGEX).hasMatch(value);
      if (value.isEmpty || !passValid) {
        return DESCRIPTION;
      } else {
        return null;
      }
    };
  }
}