import 'package:flutter/cupertino.dart';
import 'package:storybook/widgets/textformfields/validators/field_error_validator.dart';

class NonRequiredFieldValidatorImpl extends FieldErrorValidatorImpl {
  NonRequiredFieldValidatorImpl(String fieldErrorDescription) : super(fieldErrorDescription);

  String get description => fieldErrorDescription;

  FormFieldValidator<String> get validator {
    return (String value) {
      if (fieldErrorDescription == null) {
        return null;
      } else {
        return fieldErrorDescription;
      }
    };
  }
}