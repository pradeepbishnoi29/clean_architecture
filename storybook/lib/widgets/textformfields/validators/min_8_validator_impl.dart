import 'package:flutter/cupertino.dart';
import 'string_validator.dart';

class Min8ValidatorImpl extends StringValidator {
  static const String DESCRIPTION = "Minimum 8 characters";
  static const String REGEX = r'.{8,}$';

  String get description => DESCRIPTION;

  FormFieldValidator<String> get validator {
    return (String value) {
      bool passValid = RegExp('^'+REGEX).hasMatch(value);
      if (value.isEmpty || !passValid) {
        return DESCRIPTION;
      } else {
        return null;
      }
    };
  }
}