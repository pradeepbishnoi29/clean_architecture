import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storybook/widgets/textformfields/validators/aggregate_validator.dart';
import 'package:storybook/widgets/textformfields/validators/field_error_validator.dart';
import 'package:storybook/widgets/textformfields/validators/includes_1_lowercase_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/includes_1_number_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/includes_1_uppercase_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/min_8_validator_impl.dart';
import 'package:storybook/widgets/textformfields/validators/string_validator.dart';
import 'package:storybook/resources/spacer.dart';
import 'package:storybook/resources/themes.dart';
import 'package:storybook/resources/icons.dart';

class PasswordValidatorImpl extends FieldErrorValidatorImpl implements AggregateValidator {
  static const String PASSWORD_REQUIRED = "Password Required";
  static const String DESCRIPTION = "Password doesn't meet requirements";
  static const double INDIVIDUAL_REQUIREMENT_ROW_HEIGHT = 19.0;

  List<StringValidator> validators = [];

  PasswordValidatorImpl([String fieldErrorDescription]) : super(fieldErrorDescription);

  List<String> validationResults(String value) {
    Iterable<String> results = validators.map((StringValidator funcs) {
      return funcs.validator(value);
    });

    return results.toList();
  }

  Widget validationRequirementsView(String value) {
    return _getPasswordReqWidgets(validationResults(value));
  }

  Widget _getPasswordReqWidgets(List<String> strings) {
    List<Widget> list = new List<Widget>();
    for(var i = 0; i < strings.length; i++){
      list.add(_getRequirementDisplay(strings[i] == null, validators[i].description));
    }
    return new Column(children: list);
  }

  Widget _getRequirementDisplay(bool passed, String description) {
    List<Widget> list = new List<Widget>();
    DLTPasswordRequirementsTheme theme = DLTPasswordRequirementsTheme();
    if (passed) {
      list.add(DltIcons.checkCircle(theme.passColor, size: INDIVIDUAL_REQUIREMENT_ROW_HEIGHT));
    } else {
      list.add(DltIcons.error(color:theme.errorColor, size: INDIVIDUAL_REQUIREMENT_ROW_HEIGHT));
    }
    list.add(Padding(
      padding: const EdgeInsets.only(left: SPACER_8),
      child: Text(description, style: theme.primaryTextStyle),
    ));
    return new Row(children: list);
  }

  String get description => DESCRIPTION;

  FormFieldValidator<String> get validator {
    return (String value) {
      bool passValid = false;
      if(validators.isEmpty) {
        String pattern = '^'
            +Includes1UppercaseValidatorImpl.REGEX
            +Includes1LowercaseValidatorImpl.REGEX
            +Includes1NumberValidatorImpl.REGEX
            +Min8ValidatorImpl.REGEX;
        // In case we need to add special characters to the regex -- (?=.*?[!@#\$&*~])
        passValid = new RegExp(pattern).hasMatch(value);
      } else {
        Iterable<bool> results = validators.map((StringValidator funcs) {
          return funcs.validator(value) == null;
        });
        passValid = !results.contains(false);
      }

      if (value.isEmpty) {
        return PASSWORD_REQUIRED;
      } else if (!passValid) {
        return DESCRIPTION;
      } else {
        return fieldErrorDescription;
      }
    };
  }
}