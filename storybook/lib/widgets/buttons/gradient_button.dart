import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../resources/colors.dart';
import '../../resources/styles.dart';

class GradientButton extends StatelessWidget {
  final String buttonText;
  final TextStyle buttonTextStyle;
  final List<Color> gradientColors;
  final double width;
  final double height;
  final double fontSize;
  final Function onTapButton;
  final double elevation;
  final EdgeInsets margin;

  // defaults
  static const double BUTTON_RADIUS = 3.0;
  static const Color TEXT_COLOR = Colors.white;
  static const String BUTTON_TEXT = 'BUTTON';
  static const double BUTTON_HEIGHT = 40.0;
  static const double BUTTON_WIDTH = 85.0;
  static const EdgeInsets MARGIN = const EdgeInsets.all(0.0);

  GradientButton({Key key, this.buttonText, this.buttonTextStyle, this.gradientColors, this.margin,
      this.onTapButton, double width, double height, this.fontSize, this.elevation})
      : this.width = width ?? BUTTON_WIDTH,
        this.height = height ?? BUTTON_HEIGHT,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    double phoneTextScaleFactor = MediaQuery.of(context).textScaleFactor;
    double maxTextScaleFactor = 1;
    var currentTextScaleFactor = phoneTextScaleFactor > maxTextScaleFactor ? maxTextScaleFactor : phoneTextScaleFactor;
    return Container(
      margin: margin ?? MARGIN,
      child: RaisedButton(
        elevation: elevation,
        onPressed: onTapButton,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(BUTTON_RADIUS)),
        padding: onTapButton == null ? const EdgeInsets.all(2) : const EdgeInsets.all(0.0),
        disabledElevation: 2,
        child: Ink(
          decoration: onTapButton == null? _disabledBoxDecoration() : _enabledBoxDecoration(gradientColors),
          child: Container(
            height: height,
            width: width,
            alignment: Alignment.center,
            child: Text(
              buttonText,
              textScaleFactor: currentTextScaleFactor,
              textAlign: TextAlign.center,
              style: onTapButton == null? DLTStyles.CTA_INACTIVE : buttonTextStyle,
            ),
          ),
        ),
      ),
    );
  }

  BoxDecoration _disabledBoxDecoration() {
    return BoxDecoration(
        boxShadow: [BoxShadow(color: DltColors.gray4[900], blurRadius: 5.0, spreadRadius: 5.0,),],
        borderRadius: BorderRadius.all(Radius.circular(BUTTON_RADIUS)),
        color: DltColors.ctaDisabledButton);
  }

  BoxDecoration _enabledBoxDecoration(List<Color> gradientColors) {
    return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(GradientButton.BUTTON_RADIUS)),
        gradient: LinearGradient(colors: gradientColors));
  }
}

/// Gradient Button implementations
///
class GradientButtonLight extends GradientButton {
  GradientButtonLight({Key key, String buttonText, double elevation,double width, Function onTapButton}) :
        super(key: key, buttonText: buttonText ?? 'LABEL',
          buttonTextStyle: DLTStyles.CALL_TO_ACTION_BLACK,
          width: width,
          elevation: elevation,
          gradientColors: <Color>[DltColors.white[700], DltColors.white[500]],
          onTapButton: onTapButton);
}

class GradientButtonCta extends GradientButton {
  GradientButtonCta({Key key, String buttonText, Function onTapButton}) :
        super(key: key, buttonText: buttonText ?? 'LABEL',
          buttonTextStyle: DLTStyles.CALL_TO_ACTION_WHITE,
          gradientColors: <Color>[DltColors.red[800], DltColors.red[900]],
          onTapButton: onTapButton);
}

class GradientButtonDark extends GradientButton {
  GradientButtonDark(
      {Key key, String buttonText, TextStyle buttonTextStyle, double elevation, Function onTapButton, double width, EdgeInsets margin})
      :
        super(key: key,
          buttonText: buttonText ?? 'LABEL',
          width: width,
          margin: margin,
          elevation: elevation,
          buttonTextStyle: buttonTextStyle ?? DLTStyles.CALL_TO_ACTION_WHITE,
          gradientColors: <Color>[DltColors.black[700], DltColors.black[900]],
          onTapButton: onTapButton);
}

class GradientButtonDisabled extends GradientButton {
  GradientButtonDisabled({Key key, String buttonText}) :
        super(key: key, buttonText: buttonText ?? 'LABEL',
          buttonTextStyle: DLTStyles.CTA_INACTIVE,
          gradientColors: <Color>[DltColors.black[700], DltColors.white[500]],
          onTapButton: null,
          elevation: 0);
  @override
  BoxDecoration _disabledBoxDecoration() {
    return BoxDecoration(
        boxShadow: [BoxShadow(color: DltColors.gray4[900], blurRadius: 5.0, spreadRadius: 5.0,),],
        borderRadius: BorderRadius.all(Radius.circular(GradientButton.BUTTON_RADIUS)),
        color: DltColors.ctaDisabledButton);
  }
}