// Copyright 2014 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:storybook/resources/colors.dart';
import 'package:storybook/resources/icons.dart';
import 'package:storybook/resources/spacer.dart';
import 'package:storybook/resources/styles.dart';

const Duration _snackBarTransitionDuration = Duration(milliseconds: 250);
const Duration _snackBarDisplayDuration = Duration(milliseconds: 4000);
const Curve _snackBarHeightCurve = Curves.fastOutSlowIn;
const Curve _snackBarFadeInCurve = Interval(0.45, 1.0, curve: Curves.fastOutSlowIn);
const Curve _snackBarFadeOutCurve = Interval(0.72, 1.0, curve: Curves.fastOutSlowIn);

enum AlertType {ERROR, WARNING, SUCCESS}

class AlertAction extends SnackBarAction {
  final String label;
  final VoidCallback onPressed;

  AlertAction({Key key, @required this.label, @required this.onPressed})
  : super(key: key, textColor: DltColors.white, disabledTextColor: DltColors.disabledButton, label: label, onPressed: onPressed);
}

class AlertSnackBar extends SnackBar {
  /// Creates a floating snack bar for app alerts/notifications.
  ///
  /// The [content] argument must be non-null.
  const AlertSnackBar({
    Key key,
    @required this.content,
    this.action,
    this.duration = _snackBarDisplayDuration,
    this.animation,
    this.onVisible,
    this.type,
    this.additionalBottomMargin = 0.0
  }) :  assert(content != null),
        assert(duration != null),
        assert(additionalBottomMargin != null),
        super(key: key, content: content);

  static const Duration ShortDuration = _snackBarDisplayDuration;
  static const Duration MediumDuration = Duration(seconds: 7);
  static const Duration LongDuration = Duration(seconds: 10);

  final AlertType type;

  final double additionalBottomMargin;

  final Widget content;

  /// (optional) An action that the user can take based on the snack bar.
  ///
  /// For example, the snack bar might let the user undo the operation that
  /// prompted the snackbar. Snack bars can have at most one action.
  ///
  /// The action should not be "dismiss" or "cancel".
  final AlertAction action;

  /// The amount of time the snack bar should be displayed.
  ///
  /// Defaults to 4.0s.
  ///
  /// See also:
  ///
  ///  * [ScaffoldState.removeCurrentSnackBar], which abruptly hides the
  ///    currently displayed snack bar, if any, and allows the next to be
  ///    displayed.
  ///  * <https://material.io/design/components/snackbars.html>
  final Duration duration;

  /// The animation driving the entrance and exit of the snack bar.
  final Animation<double> animation;

  /// For accessibility: called the first time that the snackbar is visible within a [Scaffold].
  final VoidCallback onVisible;

  /// Creates an animation controller useful for driving a snack bar's entrance and exit animation.
  static AnimationController createAnimationController({ @required TickerProvider vsync }) {
    return AnimationController(
      duration: _snackBarTransitionDuration,
      debugLabel: 'SnackBar',
      vsync: vsync,
    );
  }

  /// Creates a copy of this snack bar but with the animation replaced with the given animation.
  ///
  /// If the original snack bar lacks a key, the newly created snack bar will
  /// use the given fallback key.
  AlertSnackBar withAnimation(Animation<double> newAnimation, { Key fallbackKey }) {
    return AlertSnackBar(
      key: key ?? fallbackKey,
      content: content,
      action: action,
      duration: duration,
      additionalBottomMargin: additionalBottomMargin,
      animation: newAnimation,
      onVisible: onVisible,
      type: type,
    );
  }

  @override
  State<AlertSnackBar> createState() => _SnackBarState();
}

class _SnackBarState extends State<AlertSnackBar> {

  /// Builds the snack bar widget
  Widget _buildSnackBar() {
    return ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 375),
        child: Container(
          margin: EdgeInsets.only(bottom: SPACER_8 + this.widget.additionalBottomMargin),
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(20, 16, 20, 16),
                decoration: BoxDecoration(
                    color: Colors.grey[850],
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(3.0),
                    boxShadow: [
                      BoxShadow(
                          color: DltColors.gray2[900],
                          offset: Offset(0, 5),
                          blurRadius: 5.0
                      )
                    ]
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //SizedBox(width: snackBarPadding),
                    Expanded(
                      child: Container(
                        child: DefaultTextStyle(
                          style: DLTStyles.LABEL_WHITE,
                          child: widget.content,
                        ),
                      ),
                    ),
                    (widget.action != null)?
                    ButtonTheme(
                      textTheme: ButtonTextTheme.accent,
                      minWidth: 64.0,
                      padding: EdgeInsets.only(left: 16),
                      child: widget.action,
                    ) :
                    SizedBox(width: 16),
                  ],
                ),
              ),
              Visibility(

                visible: (widget.type == null)? false : true,
                child: Positioned(
                  left: 15,
                  top: -20,
                  child: CircleAvatar(
                    foregroundColor: DltColors.white,
                    backgroundColor: Colors.grey[850],
                    child: _showAlertIcon(),
                  ),
                ),
              )
            ],
            overflow: Overflow.visible,
          ),
        )
    );
  }

  /// Display an icon corresponding to any of the three alert types
  Widget _showAlertIcon() {
    final double iconSize = 24.0;
    switch(widget.type) {
      case AlertType.ERROR:
        return DltIcons.errorAlert(size: iconSize);
      case AlertType.WARNING:
        return DltIcons.warningAlert(size: iconSize);
      case AlertType.SUCCESS:
      default:
        return DltIcons.successAlert(size: iconSize);
    }
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQueryData = MediaQuery.of(context);
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;
    final SnackBarThemeData snackBarTheme = theme.snackBarTheme;
    final bool isThemeDark = theme.brightness == Brightness.dark;

    // SnackBar uses a theme that is the opposite brightness from
    // the surrounding theme.
    final Brightness brightness = isThemeDark ? Brightness.light : Brightness.dark;
    final Color themeBackgroundColor = isThemeDark
        ? colorScheme.onSurface
        : Color.alphaBlend(colorScheme.onSurface.withOpacity(0.80), colorScheme.surface);
    final ThemeData inverseTheme = ThemeData(
      brightness: brightness,
      backgroundColor: themeBackgroundColor,
      colorScheme: ColorScheme(
        primary: colorScheme.onPrimary,
        primaryVariant: colorScheme.onPrimary,
        // For the button color, the spec says it should be primaryVariant, but for
        // backward compatibility on light themes we are leaving it as secondary.
        secondary: isThemeDark ? colorScheme.primaryVariant : colorScheme.secondary,
        secondaryVariant: colorScheme.onSecondary,
        surface: colorScheme.onSurface,
        background: themeBackgroundColor,
        error: colorScheme.onError,
        onPrimary: colorScheme.primary,
        onSecondary: colorScheme.secondary,
        onSurface: colorScheme.surface,
        onBackground: colorScheme.background,
        onError: colorScheme.error,
        brightness: brightness,
      ),
      snackBarTheme: snackBarTheme,
    );

    final SnackBarBehavior snackBarBehavior = SnackBarBehavior.floating;
    final bool isFloatingSnackBar = snackBarBehavior == SnackBarBehavior.floating;

    final CurvedAnimation heightAnimation = CurvedAnimation(parent: widget.animation, curve: _snackBarHeightCurve);
    final CurvedAnimation fadeInAnimation = (widget.animation != null) ? CurvedAnimation(parent: widget.animation, curve: _snackBarFadeInCurve) : null;
    final CurvedAnimation fadeOutAnimation = (widget.animation != null) ? CurvedAnimation(
      parent: widget.animation,
      curve: _snackBarFadeOutCurve,
      reverseCurve: const Threshold(0.0),
    ) : null;

    Widget snackBar = SafeArea(
      top: false,
      bottom: !isFloatingSnackBar,
      child: _buildSnackBar()
    );

    snackBar = Material(
      child: Theme(
        data: inverseTheme,
        child: mediaQueryData.accessibleNavigation
            ? snackBar
            : FadeTransition(
          opacity: fadeOutAnimation,
          child: snackBar,
        ),
      ),
    );

    if (isFloatingSnackBar) {
      snackBar = Padding(
        padding: const EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 10.0),
        child: snackBar,
      );
    }

    snackBar = Semantics(
      container: true,
      liveRegion: true,
      onDismiss: () {
        Scaffold.of(context).removeCurrentSnackBar(reason: SnackBarClosedReason.dismiss);
      },
      child: Dismissible(
        key: const Key('dismissible'),
        direction: DismissDirection.down,
        resizeDuration: null,
        onDismissed: (DismissDirection direction) {
          Scaffold.of(context).removeCurrentSnackBar(reason: SnackBarClosedReason.swipe);
        },
        child: snackBar,
      ),
    );

    Widget snackBarTransition;
    if (mediaQueryData.accessibleNavigation) {
      snackBarTransition = snackBar;
    } else if (isFloatingSnackBar) {
      snackBarTransition = FadeTransition(
        opacity: fadeInAnimation,
        child: snackBar,
      );
    } else {
      snackBarTransition = AnimatedBuilder(
        animation: heightAnimation,
        builder: (BuildContext context, Widget child) {
          return Align(
            alignment: AlignmentDirectional.topStart,
            heightFactor: heightAnimation.value,
            child: child,
          );
        },
        child: snackBar,
      );
    }

    return ClipRect(child: snackBarTransition);
  }
}
