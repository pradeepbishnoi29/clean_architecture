import 'package:flutter/material.dart';
import 'package:storybook/resources/colors.dart';
import 'package:storybook/resources/styles.dart';

class LabeledSwitch extends StatelessWidget {
  final String label;
  final bool isSelected;
  final Function onChanged;
  final EdgeInsets padding;

  LabeledSwitch({this.label, this.isSelected, this.onChanged, this.padding});

  // defaults
  static const double WIDTH = 250.0;
  static const double HEIGHT = 40.0;
  static const SWITCH_PADDING = const EdgeInsets.symmetric(horizontal: 4);
  static const LABEL_PADDING = const EdgeInsets.only(left: 4);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: WIDTH,
      alignment: Alignment.center,
      padding: padding ?? SWITCH_PADDING,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Switch(
            activeColor: DltColors.accentColorBlue,
            value: isSelected,
            onChanged: (bool newValue) {
              if (onChanged != null) {
                onChanged(newValue);
              }
            },
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: LABEL_PADDING,
              child: Text(
                label,
                style: DLTStyles.LABEL_BLACK,
              ),
            ),
          )
        ],
      ),
    );
  }
}