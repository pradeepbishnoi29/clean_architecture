import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:storybook/resources/colors.dart';
import 'package:storybook/resources/styles.dart';

import '../../resources/spacer.dart';

class LabeledCheckbox extends StatefulWidget {
  final String label;
  final EdgeInsets padding;
  final Function onChanged;
  final bool initialCheckState;
  final double width;

  LabeledCheckbox(
      {Key key,
      @required this.label,
      @required this.onChanged,
      this.padding,
      this.width,
      this.initialCheckState = false})
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _LabeledCheckboxState(label, padding, onChanged, initialCheckState);
}

class _LabeledCheckboxState extends State<LabeledCheckbox> {
  final String label;
  final EdgeInsets padding;
  final Function onChanged;
  bool _isChecked = false;

  // defaults
  static double width = 275.0;
  static const CHECKBOX_PADDING = const EdgeInsets.symmetric(horizontal: 4);

  _LabeledCheckboxState(
      this.label, this.padding, this.onChanged, this._isChecked);

  @override
  void initState() {
    width = widget.width ?? width;
    super.initState();
  }

  void _toggleCheckbox() {
    if (!_isChecked) {
      setState(() {
        _isChecked = true;
      });
    } else {
      setState(() {
        _isChecked = false;
      });
    }

    // update user callback
    if (onChanged != null) {
      onChanged(_isChecked);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Semantics( child: InkWell(
      onTap: () {},
      child: Container(
        width: width,
        //height: HEIGHT,
        padding: padding ?? CHECKBOX_PADDING,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
                height: 24.0,
                width: 24.0,
                child: IndexedSemantics(index: 2,
                    child: Semantics(excludeSemantics: true,
                      label: _isChecked
                          ? 'Checkbox selected'
                          : 'Checkbox unselected',
                      child: Checkbox(
                        activeColor: DltColors.black,
                        value: _isChecked,
                        onChanged: (value) {
                          _toggleCheckbox();
                        },
                      ),
                    ))),
            Expanded(
              child: Padding(
                  padding: EdgeInsets.only(left: SPACER_8),
                  child: IndexedSemantics(
                      index: 1,
                      child: Semantics(excludeSemantics: true, child: Text(
                        label,
                        style: DLTStyles.LABEL_BLACK,
                      )))),
            ),
          ],
        ),
      ),
    ));
  }
}
