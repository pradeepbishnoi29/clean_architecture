import 'package:flutter/material.dart';
import 'package:storybook/resources/colors.dart';
import 'package:storybook/resources/styles.dart';

class LabeledRadioButton<T> extends StatelessWidget {
  final String label;
  final T value;
  final T groupValue;
  final ValueChanged onChanged;

  // defaults
  static const double WIDTH = 250.0;
  static const double HEIGHT = 40.0;

  LabeledRadioButton({@required this.label, this.value, this.groupValue, this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: WIDTH,
      height: HEIGHT,
      child: RadioListTile(
        activeColor: DltColors.black,
        selected: true,
        value: value,
        groupValue: groupValue,
        onChanged: onChanged,
        title: Text(label, style: DLTStyles.LABEL_BLACK),
      ),
    );
  }
}