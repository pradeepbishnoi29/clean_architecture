class StorybookKeys {
  // icons
  static const String closeIcon= 'closeIcon';

  // form fields
  static const String emailFormField = 'emailFormField';
  static const String passwordFormField = 'passwordFormField';
  static const String firstNameFormField = 'firstNameFormField';
  static const String lastNameFormField = 'lastNameFormField';
  static const String promoIDFormField = 'promoIDFormField';
  static const String address1FormField = 'address1FormField';
  static const String address2FormField = 'address2FormField';
  static const String cityFormField = 'cityFormField';
  static const String stateFormField = 'stateFormField';
  static const String postalFormField = 'postalFormField';
  static const String companyFormField = 'companyFormField';
  static const String mobileFormField = 'mobileFormField';

  // buttons
  static const String clearFormFieldButton = 'clearFormFieldButton';
  static const String obscurityToggleButton = 'obscurityToggleButton';

  // used for storybook
  static const String cardCarouselProduct = 'cardCarouselProduct';
  static const String cardCarouselImage = 'cardCarouselImage';
}