// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'injector.dart';

// **************************************************************************
// InjectorGenerator
// **************************************************************************

class _$Injector extends Injector {
  void _configureCommon() {
    final Container container = Container();


    container.registerFactory((c) => SharedPreferenceHelper());
    //Token
    container.registerFactory((c) => GetTokenUseCase(c<TokenRepository>()));
    container.registerFactory<TokenRepository, TokenRepositoryImpl>((c) => TokenRepositoryImpl(c<TokenRemoteClient>(), c<TokenCacheClient>()));
    container.registerFactory<TokenRemoteClient, TokenRemoteClientImpl>((c) => TokenRemoteClientImpl());
    container.registerFactory<TokenCacheClient, TokenCacheClientImpl>((c) => TokenCacheClientImpl(c<SharedPreferenceHelper>()));

    container.registerFactory((c) => MessageUseCase(c<MessageRepository>()));
    container.registerFactory<MessageRepository, MessageRepositoryImpl>((c) => MessageRepositoryImpl(c<MessageRemoteClient>()));
    container.registerFactory<MessageRemoteClient, MessageRemoteClientImpl>((c) => MessageRemoteClientImpl());
  }
}
