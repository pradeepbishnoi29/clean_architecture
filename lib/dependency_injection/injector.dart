import 'package:data/repositoryImpl/message_repository_impl.dart';
import 'package:data/repositoryImpl/remote_client/message_remote_client_impl.dart';
import 'package:data/repositoryImpl/cache_client/token_cache_client_impl.dart';
import 'package:data/repositoryImpl/remote_client/token_remote_client_impl.dart';
import 'package:data/repositoryImpl/token_repository_impl.dart';
import 'package:data/sharedpref/shared_preference_helper.dart';
import 'package:domain/repository/message_repository.dart';
import 'package:domain/repository/token_repository.dart';
import 'package:domain/usecases/get_message_usecase.dart';
import 'package:domain/usecases/get_token_usecase.dart';
import 'package:kiwi/kiwi.dart';

part 'injector.g.dart';

abstract class Injector {
  static Container container;

  static final resolve = container.resolve;

  static void setup({bool isProduction}) {
    container = Container();
    var injector = _$Injector();
    injector._configureCommon();
  }

  // Token
  @Register.factory(GetTokenUseCase)
  @Register.factory(TokenRepository, from: TokenRepositoryImpl)
  @Register.factory(TokenRemoteClient, from: TokenRemoteClientImpl)
  @Register.factory(TokenCacheClient, from: TokenCacheClientImpl)

  @Register.factory(MessageUseCase)
  @Register.factory(MessageRepository, from: MessageRepositoryImpl)
  @Register.factory(MessageRemoteClient, from: MessageRemoteClientImpl)

  // SharedPreference
  @Register.singleton(SharedPreferenceHelper)

  void _configureCommon();
}