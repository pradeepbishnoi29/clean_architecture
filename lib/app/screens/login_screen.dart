import 'package:flutter/material.dart';
import 'package:logincleanarchitecture/app/utils/alert_type_utils.dart';
import 'package:logincleanarchitecture/app/utils/field_error_utils.dart';
import 'package:logincleanarchitecture/app/view_model/base_view_model.dart';

import 'package:logincleanarchitecture/app/view_model/token_view_model.dart';
import 'package:data/utils/logger.dart';
import 'package:storybook/widgets/snackbar/alert_snackbar.dart';
import 'package:storybook/widgets/utils/keys.dart';
import 'package:storybook/widgets/buttons/gradient_button.dart';
import 'package:storybook/widgets/textformfields/helpers/text_form_field_property_bundle.dart';
import 'package:storybook/widgets/textformfields/helpers/text_form_field_type_enumeration.dart';
import 'package:storybook/widgets/textformfields/validating_textformfield_factory.dart';
import 'package:storybook/resources/spacer.dart';
import 'package:storybook/resources/styles.dart';
import 'package:provider/provider.dart';


class AuthScreen extends StatefulWidget {
  final Key key;

  AuthScreen({@required this.key});

  @override
  State<StatefulWidget> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final String logTag = '_AuthScreenState';
  bool _loginFieldState = false;
  bool _runLoginValidation = false;

  final loginUserController = TextEditingController(text: "eve.holt@reqres.in");
  final loginPassController = TextEditingController(text: "cityslicka");
  final _loginFormKey = GlobalKey<FormState>();

  var _fieldErrorMap;


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<TokenViewModel>(create: (context) => TokenViewModel(),
        child: Scaffold(
          body: Consumer<TokenViewModel>(builder: (context, tokenModel, child) {
            return Center(child:_buildLogIn(context, tokenModel));
          }),
        ));
  }


  void _testLoginFormValidation() {
    setState(() {
      Log.d(logTag, "RETESTING LOGIN");
      _loginFieldState = (_loginFormKey.currentState == null) ? true : _loginFormKey.currentState.validate();
    });
  }

  void _closeScreen(BuildContext context) {
    Navigator.pop(context);
  }

  void _showAlertMessage(BuildContext context,  {String message  = 'Nothing to display', AlertType alertType = AlertType.SUCCESS}) {
    Scaffold.of(context).showSnackBar(
        AlertSnackBar(content: Text(message), type: alertType, duration: AlertSnackBar.ShortDuration));
  }

  Widget _buildLogIn(BuildContext context, TokenViewModel model) {
    // capture field errors
    _fieldErrorMap = populateFieldErrors(model.error);

    if(!_runLoginValidation) {
      _runLoginValidation = true;
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _testLoginFormValidation();
      });
    }

    var w;
    var _fieldChanged = (String _){
      _testLoginFormValidation();
    };
    var responseStatus = model.responseStatus;
    Log.d(logTag, "BUILDING LOGIN $responseStatus");

    switch (responseStatus) {
      case ResponseStatus.SUCCESSFUL:
        WidgetsBinding.instance.addPostFrameCallback((_) => _showAlertMessage(context, message: 'Successful login', alertType: AlertType.SUCCESS));
        break;
      case ResponseStatus.ERROR:
        // display error message
        AlertType alertType = AlertUtils.getAlertForError(model.error);
        WidgetsBinding.instance.addPostFrameCallback((_) => _showAlertMessage(context, message: model.error?.errorMessage ?? 'Nothing to display' , alertType: alertType));

        continue displayForm;
      case ResponseStatus.LOADING:
        w = CircularProgressIndicator(backgroundColor: Colors.red);
        break;
      displayForm:
      default:
        w = Form(
            key: _loginFormKey,
            child: Padding(
              padding: const EdgeInsets.only(
                  top: SPACER_32, left: SPACER_24, right: SPACER_24),
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      'HAVE AN ACCOUNT?\nGET IN HERE.',
                      style: DLTStyles.HEADLINE_1_RED,
                      textAlign: TextAlign.left,
                    ),
                  ),
                  generateSpacer(SPACER_24),
                  ValidatingTextFormFieldFactory.fromStyle(
                      key: Key(StorybookKeys.emailFormField),
                      textFormFieldType: TextFormFieldTypeEnumeration.EMAIL,
                      textformfieldbundle: TextFormFieldPropertyBundle(
                          controller: loginUserController,
                          fieldError: getFieldError(_fieldErrorMap, FieldErrorType.EMAIL),
                          onChanged: _fieldChanged,
                          onSubmitted: _fieldChanged)),
                  generateSpacer(SPACER_40),
                  ValidatingTextFormFieldFactory.fromStyle(
                      key: Key(StorybookKeys.passwordFormField),
                      textFormFieldType: TextFormFieldTypeEnumeration.REQUIRED,
                      textformfieldbundle: TextFormFieldPropertyBundle(
                        controller: loginPassController,
                        fieldError: getFieldError(_fieldErrorMap, FieldErrorType.PASSWORD),
                        onChanged: _fieldChanged,
                        onSubmitted: _fieldChanged,
                        label: "Password",
                        obscureText: true,
                      )),
                  generateSpacer(SPACER_48),
                  Row(
                    children: <Widget>[
                      Flexible(
                          child: Align(
                              alignment: Alignment.centerRight,
                              child: GradientButtonLight(
                                  buttonText: 'LOG IN',
                                  onTapButton: _loginFieldState ? () {
                                    if (_loginFormKey.currentState.validate()) {
                                      _loginFormKey.currentState.save();

                                      Provider.of<TokenViewModel>(context, listen: false).login(loginUserController.text, loginPassController.text);

                                    }
                                  }
                                      : null
                              )))
                    ],
                  ),
                ],
              ),
            ));
    }
    return w;
  }
}
