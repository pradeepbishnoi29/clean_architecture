import 'package:domain/entities/error_bundle.dart';
import 'package:domain/entities/token.dart';
import 'package:domain/usecases/get_token_usecase.dart';
import 'package:data/utils/logger.dart';
import 'package:logincleanarchitecture/app/view_model/base_view_model.dart';
import 'package:logincleanarchitecture/dependency_injection/injector.dart';


class TokenViewModel extends BaseViewModel<Token> {
  final String logTag = 'TokenViewModel';
  GetTokenUseCase _loginUseCase;

  TokenViewModel() {
    _loginUseCase = Injector.resolve<GetTokenUseCase>();
  }

  Token getToken() {
    return dataModel;
  }

  void _saveToken() {
    _loginUseCase.saveToken(dataModel);
  }


  void login(String username, String password) {
    var future = _loginUseCase.getToken(username, password);
    future.then((token) {
      responseStatus = ResponseStatus.SUCCESSFUL;
      dataModel = token;
      Log.d(logTag, "SUCCESSS " + token.toString());
      notifyListeners();
      _saveToken();
    }).catchError((e) {
        var newToken = Token(token: "");
        error = e;
        dataModel = newToken;
        if (e is ErrorBundle) Log.e(logTag, 'Error: ' + e.errorMessage);
        responseStatus = ResponseStatus.ERROR;
        notifyListeners();
    });

     //loading...
    responseStatus = ResponseStatus.LOADING;
    notifyListeners();
  }
}