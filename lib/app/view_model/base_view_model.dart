import 'package:domain/entities/base_model.dart';
import 'package:domain/entities/error_bundle.dart';
import 'package:flutter/foundation.dart';

enum ResponseStatus {
  SUCCESSFUL,
  ERROR,
  LOADING
}

class BaseViewModel<T extends BaseModel> extends ChangeNotifier {
  T dataModel;
  ErrorBundle error;
  ResponseStatus responseStatus;

  void resetStatus(){
    responseStatus = null;
    notifyListeners();
  }
}