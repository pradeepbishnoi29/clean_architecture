import 'package:flutter/material.dart';
import 'package:logincleanarchitecture/app/navigation/routes.dart';
import 'package:logincleanarchitecture/app/screens/home_screen.dart';
import 'package:logincleanarchitecture/app/screens/login_screen.dart';
import 'package:logincleanarchitecture/app/utils/keys.dart';


class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case DltRoutes.home:
        return PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) => HomeScreen(key: Key(Keys.homeScreen)),
            transitionsBuilder: (context, animation, secondaryAnimation, child) => child
        );
      default:
      return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => AuthScreen(key: Key(Keys.authenticationScreen)),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            var begin = Offset(0.0, 1.0);
            var end = Offset.zero;
            var curve = Curves.ease;
            var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

            return SlideTransition(
              position: animation.drive(tween),
              child: child,
            );
          }
      );

    }
  }
}