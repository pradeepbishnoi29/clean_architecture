import 'package:domain/entities/error_bundle.dart';
import 'package:storybook/widgets/snackbar/alert_snackbar.dart';

/// Utility class for Alert message related tasks
class AlertUtils {
  static AlertType getAlertForError(ErrorBundle errorBundle) {
      if (errorBundle == null) return AlertType.SUCCESS;
      if (errorBundle.errorLevel == 'warning') {
        return AlertType.WARNING;
      }
      return AlertType.ERROR;
  }
}
