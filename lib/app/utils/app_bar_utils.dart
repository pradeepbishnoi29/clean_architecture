import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppBarUtils {
  static Widget getDltAppBarTitle() {
    return SvgPicture.asset('assets/gnc_logo.svg',
        fit: BoxFit.contain, width: 28, height: 28);
  }
}