import 'dart:collection';

import 'package:domain/entities/error_bundle.dart';

enum FieldErrorType {
  FIRST_NAME, EMAIL, PASSWORD, PROMOID, LAST_NAME, ADDRESS_1, ADDRESS_2, COMPANY_NAME, CITY, STATE, POSTAL_CODE, MOBILE_NUMBER
}

Map<FieldErrorType, String> populateFieldErrors(ErrorBundle errorBundle) {
  if (errorBundle == null) return null;
  Map<FieldErrorType, String> map = HashMap<FieldErrorType, String>();

  errorBundle.fieldErrors?.forEach((fieldError) {
    FieldErrorType errorType;
    if (fieldError.id == "email") {
      errorType = FieldErrorType.EMAIL;
    }
    else if (fieldError.id == "firstName") {
      errorType = FieldErrorType.FIRST_NAME;
    }
    else if (fieldError.id == 'password') {
      errorType = FieldErrorType.PASSWORD;
    }
    else if (fieldError.id == 'promoId') {
      errorType = FieldErrorType.PROMOID;
    }
    else if (fieldError.id == "lastName") {
      errorType = FieldErrorType.LAST_NAME;
    }
    else if (fieldError.id == "addressLine1") {
      errorType = FieldErrorType.ADDRESS_1;
    }
    else if (fieldError.id == "addressLine2") {
      errorType = FieldErrorType.ADDRESS_2;
    }
    else if (fieldError.id == "companyName") {
      errorType = FieldErrorType.COMPANY_NAME;
    }
    else if (fieldError.id == "city") {
      errorType = FieldErrorType.CITY;
    }
    else if (fieldError.id == "state") {
      errorType = FieldErrorType.STATE;
    }
    else if (fieldError.id == "postalCode") {
      errorType = FieldErrorType.POSTAL_CODE;
    }
    else if (fieldError.id == "mobileNumber") {
      errorType = FieldErrorType.MOBILE_NUMBER;
    }
    map.putIfAbsent(errorType, () => fieldError.error);
  });

  return map;
}

String getFieldError(Map<FieldErrorType, String> map, FieldErrorType type) {
  String error = (map != null)? map[type] : null;
  return error;
}