import 'package:flutter/material.dart';
import 'package:logincleanarchitecture/app/navigation/router.dart';
import 'package:logincleanarchitecture/app/navigation/routes.dart';
import 'package:logincleanarchitecture/dependency_injection/injector.dart';

import 'package:storybook/resources/themes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  registerDependencies();
  runApp(MyApp());
}

void registerDependencies() {
  Injector.setup(isProduction: true);
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'DLT Demo',
        theme: dltTheme,
        color: Colors.white,
        // hide debug banner
        debugShowCheckedModeBanner: false,
        initialRoute: DltRoutes.authentication,
        onGenerateRoute: Router.generateRoute,
      );
  }
}