class Endpoints {
  Endpoints._();

  // base url
  static const String baseUrl = "https://qa-iot.dltlabs.com/api/1.0";

  static const String baseUrlLogin = 'https://reqres.in/api';

  // receiveTimeout
  static const int receiveTimeout = 5000;

  // connectTimeout
  static const int connectionTimeout = 3000;

  // booking endpoints
  static const String saveMessage = baseUrl + "/message";
  static const String getMessage = baseUrl + '';
  static const String login = 'https://reqres.in/api/login';

}