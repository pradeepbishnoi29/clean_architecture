import 'package:domain/repository/message_repository.dart';
import 'package:domain/entities/input_message_model.dart';
import 'package:domain/entities/success_response_model.dart';

class MessageRepositoryImpl implements MessageRepository {

  MessageRemoteClient _remoteClient;
  MessageRepositoryImpl(this._remoteClient);

  @override
  Future<List<InputMessage>> getMessages() {
    return _remoteClient.getMessage();
  }

  @override
  Future<SuccessResponseModel> saveMessage(InputMessage user) {
    return _remoteClient.saveMessage(user);
  }
}

abstract class MessageRemoteClient {
  Future<List<InputMessage>> getMessage();
  Future<SuccessResponseModel> saveMessage(InputMessage user);
}
