import 'package:data/sharedpref/shared_preference_helper.dart';
import 'package:data/utils/logger.dart';

import 'package:data/repositoryImpl/token_repository_impl.dart';
import 'package:domain/entities/token.dart';

class TokenCacheClientImpl implements TokenCacheClient {
  SharedPreferenceHelper _sharedPreferenceHelper;
  final String logTag = 'TokenCacheClientImpl';

  TokenCacheClientImpl(SharedPreferenceHelper sharedPreferenceHelper) {
    _sharedPreferenceHelper = sharedPreferenceHelper;
  }

  @override
  Future<Token> getToken() async {
    return Future.value(Token(token: await _sharedPreferenceHelper.authToken));
  }

  @override
  Future<void> saveToken(Token token) async {
    await _sharedPreferenceHelper.saveAuthToken(token.token);
  }

  @override
  Future<void> removeToken() async {
    _sharedPreferenceHelper.removeAuthToken();
  }

  @override
  Future<bool> hasCache() async {
    var token = await _sharedPreferenceHelper.authToken;
    return (token!= null && token.isNotEmpty);
  }
}