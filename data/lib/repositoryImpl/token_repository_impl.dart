import 'package:domain/repository/token_repository.dart';
import 'package:domain/entities/token.dart';

class TokenRepositoryImpl implements TokenRepository {

  TokenRemoteClient _remoteClient;
  TokenCacheClient _cacheClient;

  TokenRepositoryImpl(this._remoteClient, this._cacheClient);

  @override
  Future<Token> getRemoteToken(String username, String password) {
    return _remoteClient.getUserToken(username, password);
  }

  @override
  Future<Token> getLocalToken(){
    return _cacheClient.getToken();
  }

  @override
  Future<void> saveToken(Token token) {
    return _cacheClient.saveToken(token);
  }

  @override
  Future<void> removeLocalToken() {
    return _cacheClient.removeToken();
  }

  @override
  Future<bool> hasLocalToken() {
    return _cacheClient.hasCache();
  }
}

abstract class TokenRemoteClient {
  Future<Token> getUserToken(String username, String password);
}

abstract class TokenCacheClient {
  Future<Token> getToken();
  Future<void> saveToken(Token token);
  Future<void> removeToken();
  Future<bool> hasCache();
}