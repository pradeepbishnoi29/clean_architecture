import 'package:data/mappers/domainModelMappers/token_model_mapper.dart';
import 'package:data/network/constants/endpoints.dart';
import 'package:data/network/rest_client.dart';
import 'package:data/repositoryImpl/remote_client/base_remote_client.dart';
import 'package:data/repositoryImpl/token_repository_impl.dart';
import 'package:domain/entities/token.dart';

class TokenRemoteClientImpl extends BaseRemoteClientImpl implements TokenRemoteClient {

  RestClient _restClient = RestClient();

  @override
  Future<Token> getUserToken(String username, String password) {
    return _restClient.post(Endpoints.login, body: TokenModelMapper.toMap(username, password))
        .then((dynamic res) => TokenModelMapper.fromJson(res))
        .catchError((error) => handleError(error));
  }
}