import 'package:data/mappers/domainModelMappers/message_model_mapper.dart';
import 'package:data/mappers/domainModelMappers/response_model_mapper.dart';
import 'package:data/network/constants/endpoints.dart';
import 'package:data/network/rest_client.dart';
import 'package:data/repositoryImpl/remote_client/base_remote_client.dart';
import 'package:data/repositoryImpl/message_repository_impl.dart';
import 'package:domain/entities/input_message_model.dart';
import 'package:domain/entities/success_response_model.dart';

class MessageRemoteClientImpl extends BaseRemoteClientImpl implements MessageRemoteClient {

  RestClient _restClient = RestClient();

  Future<List<InputMessage>> getMessage() {
    return _restClient
        .get(Endpoints.getMessage)
        .then((dynamic res) => MessageModelMapper.listFromJson(res))
        .catchError((error) => handleError(error));
  }

  @override
  Future<SuccessResponseModel> saveMessage(InputMessage message) {
    return _restClient.post(Endpoints.saveMessage, headers: header(), body: message.toMap())
        .then((dynamic res) => ResponseModelMapper.fromJson(res))
        .catchError((error) => handleError(error));
  }
}