class Preferences {
  Preferences._();

  static const String is_logged_in = "isLoggedIn";
  static const String auth_token = "authToken";
  static const String is_dark_mode = "is_dark_mode";
  static const String current_language = "current_language";
  static const String messageType = 'messageType';
  static const String fourKitesLoadId = 'fourKitesLoadId';
  static const String loadNumber = 'loadNumber';
  static const String shipper = 'shipper';
  static const String temperatureUnit = 'temperatureUnit';
  static const String temperatureReading = 'temperatureReading';
  static const String distanceFilter = 'distanceFilter';
}