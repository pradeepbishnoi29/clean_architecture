import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants/preferences.dart';

class SharedPreferenceHelper {
  // shared pref instance
  SharedPreferences _sharedPreference;

   SharedPreferenceHelper(){
     initSharedPreference();
   }
//
//  SharedPreferenceHelper._internal() {
//    _sharedPreference = SharedPreferences.getInstance();
//  }

  initSharedPreference() async {
    _sharedPreference = await SharedPreferences.getInstance();
  }
  // General Methods: ----------------------------------------------------------
  Future<String> get authToken async {
    return _sharedPreference.getString(Preferences.auth_token);
  }

  Future<String> get messageType async {
    return _sharedPreference.getString(Preferences.messageType);
  }

  Future<String> get fourKitesLoadId async {
    return _sharedPreference.getString(Preferences.fourKitesLoadId);
  }


 Future<String> get loadNumber async {
    return _sharedPreference.getString(Preferences.loadNumber);
  }

  Future<String> get shipper async {
    return _sharedPreference.getString(Preferences.shipper);
  }

  Future<String> get temperatureUnit async {
    return _sharedPreference.getString(Preferences.temperatureUnit);
  }

  Future<String> get temperatureReading async {
    return _sharedPreference.getString(Preferences.temperatureReading);
  }

  Future<int> get distanceFilter async {
    return _sharedPreference.getInt(Preferences.distanceFilter);
  }

  Future<void> saveDistanceFilter(int distance) {
    return _sharedPreference.setInt(Preferences.distanceFilter, distance);
  }

  Future<void> saveMessageType(String messageType) {
    return _sharedPreference.setString(Preferences.messageType, messageType);
  }

  Future<void> saveFourKitesLoadId(String fourKitesLoadId) {
    return _sharedPreference.setString(Preferences.fourKitesLoadId, fourKitesLoadId);
  }

  Future<void> saveLoadNumber(String loadNumber) {
    return _sharedPreference.setString(Preferences.loadNumber, loadNumber);
  }


  Future<void> saveShipper(String shipper) {
    return _sharedPreference.setString(Preferences.shipper, shipper);
  }

  Future<void> saveTemperatureUnit(String temperatureUnit) {
    return _sharedPreference.setString(Preferences.temperatureUnit, temperatureUnit);
  }

  Future<void> saveTemperatureReading(String temperatureReading) {
    return _sharedPreference.setString(Preferences.temperatureReading, temperatureReading);
  }

  Future<void> saveAuthToken(String authToken) async {
    return _sharedPreference.setString(Preferences.auth_token, authToken);
  }

  Future<void> removeAuthToken() async {
    return _sharedPreference.remove(Preferences.auth_token);
  }

  Future<bool> get isLoggedIn async {
    return _sharedPreference.getString(Preferences.auth_token) ?? false;
  }

  // Theme:------------------------------------------------------
  Future<bool> get isDarkMode {
    return _sharedPreference.getBool(Preferences.is_dark_mode) ?? Future.value(false);
  }

  Future<void> changeBrightnessToDark(bool value) {
    return _sharedPreference.setBool(Preferences.is_dark_mode, value);
  }

  // Language:---------------------------------------------------
  String get currentLanguage {
    return _sharedPreference.getString(Preferences.current_language);
  }

  Future<void> changeLanguage(String language) {
    return _sharedPreference.setString(Preferences.current_language, language);
  }
}
