import 'mapper.dart';

/// Maps a list to another list
class ListMapperImpl<I, O> implements Mapper<List<I>, List<O>> {
  Mapper<I, O> mapper;

  ListMapperImpl(this.mapper);

  @override
  List<O> map(List<I> input) {
    if (input == null) {
      return [];
    }
    return input.map((e) => mapper.map(e)).toList();
  }
}