import 'package:domain/repository/dlt_contract.dart';
import 'package:domain/entities/success_response_model.dart';

class ResponseModelMapper {

  static SuccessResponseModel fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    var message = SuccessResponseModel();
    message.result = json[ResponseColumns.result];
    message.messageType = json[ResponseColumns.messageType];
    message.fourKitesLoadId = json[ResponseColumns.fourKitesLoadId];
    return message;
  }
}