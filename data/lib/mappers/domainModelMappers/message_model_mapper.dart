import 'package:domain/entities/input_message_model.dart';

class MessageModelMapper {
  static List<InputMessage> listFromJson(List<dynamic> json) {
    return json == null
        ? List<InputMessage>()
        : json.map((value) => MessageModelMapper.fromJson(value)).toList();
  }

  static InputMessage fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    var message = InputMessage();
    //message.messageType = json[UserColumns.messageType];
    //TODO : need to parse
    return message;
  }

  static Map<String, List<InputMessage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<InputMessage>>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) {
        map[key] = listFromJson(value);
      });
    }
    return map;
  }
}
