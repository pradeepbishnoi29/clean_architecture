import 'dart:collection';

import 'package:domain/entities/token.dart';
import 'package:domain/repository/dlt_contract.dart';

class TokenModelMapper {

  static Token fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Token(token: json[TokenColumns.token]);
  }

  static Map<String, String> toMap(String email, String password) {
    var map = HashMap<String, String>();
    map[TokenColumns.email] = email;
    map[TokenColumns.password] = password;
    return map;
  }
}