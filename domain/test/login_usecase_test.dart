import 'package:domain/repository/token_repository.dart';
import 'package:domain/usecases/get_token_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockTokenRepository extends Mock implements TokenRepository {}

void main() {
  final tokenRepository = MockTokenRepository();
  final loginUseCase = GetTokenUseCase(tokenRepository);

  test('Login usecase get-token test', () {
    String username = 'username';
    String password = 'password';
    // when a call is made to get a token
    loginUseCase.getToken(username, password);
    // verify that the get-token call is made on the token repository
    verify(tokenRepository.getRemoteToken(username, password)).called(1);
  });
}