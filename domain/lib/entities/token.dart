import 'dart:collection';

import 'package:domain/entities/base_model.dart';
import 'package:domain/repository/dlt_contract.dart';
import 'package:flutter/foundation.dart';

class Token extends BaseModel {
  String token;

  Token({@required this.token})
      : assert(token != null);

  @override
  String toString() {
    return 'Token: $token';
  }

  @override
  void fromMap(Map<String, dynamic> map) {
    this.token = map[TokenColumns.token];
  }

  @override
  String getId() => token;

  @override
  Map<String, dynamic> toMap() {
    var map = HashMap<String, dynamic>();
    map[TokenColumns.token] = token;
   return map;
  }
}