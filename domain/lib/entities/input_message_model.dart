import 'dart:collection';

import 'package:domain/entities/base_model.dart';
import 'package:domain/repository/dlt_contract.dart';

class InputMessage extends BaseModel {
   String messageType;
   String fourKitesLoadId;
   String loadNumber;
   String proNumber;
   String shipper;
   List<String> tags;
   List<String> referenceNumbers;
   String location;
   String city;
   String state;
   String country;
   String latitude;
   String longitude;
   String temperatureReading;
   String temperatureUnit;
   String sensor;
   String odometerReading;
   String timestamp;
   String timezone;
   String timezoneShortName;
   String timezoneOffset;

  InputMessage();

  @override
  String toString() {
    return 'Message fourKitesLoadId: $fourKitesLoadId';
  }

  //TO DO: NEED TO FLUSH THESE OUT

  @override
  void fromMap(Map<String, dynamic> map) {
   //TODO : parse json
  }

  @override
  Map<String, dynamic> toMap() {
    var map = HashMap<String, dynamic>();
    map[MessageColumns.messageType] = messageType;
    map[MessageColumns.fourKitesLoadId] = fourKitesLoadId;
    map[MessageColumns.loadNumber] = loadNumber;
    map[MessageColumns.proNumber] = proNumber;
    map[MessageColumns.shipper] = shipper;
    map[MessageColumns.location] = location;
    map[MessageColumns.city] = city;
    map[MessageColumns.state] = state;
    map[MessageColumns.country] = country;
    map[MessageColumns.latitude] = latitude;
    map[MessageColumns.longitude] = longitude;
    map[MessageColumns.temperatureReading] = temperatureReading;
    map[MessageColumns.temperatureUnit] = temperatureUnit;
    map[MessageColumns.sensor] = sensor;
    map[MessageColumns.odometerReading] = odometerReading;
    map[MessageColumns.timestamp] = timestamp;
    map[MessageColumns.timezone] = timezone;
    map[MessageColumns.timezoneShortName] = timezoneShortName;
    map[MessageColumns.timezoneOffset] = timezoneOffset;
    return map;
  }

  String createMessageId() {
    return '${DateTime.now().millisecondsSinceEpoch}';
  }
}