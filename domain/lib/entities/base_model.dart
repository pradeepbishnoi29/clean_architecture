import 'error_bundle.dart';

abstract class BaseModel {
  ErrorBundle errorBundle;
  void fromMap(Map<String, dynamic> map);

  Map<String, dynamic> toMap();

  Type getRuntimeType() {
    return runtimeType;
  }
}