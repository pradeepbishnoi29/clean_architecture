class ErrorBundle implements Exception {
  String errorCode;
  String errorMessage;
  String errorLevel;
  List<FieldErrors> fieldErrors;
  ErrorAction errorAction;

  ErrorBundle(this.errorCode, this.errorMessage, {this.errorLevel,
      this.fieldErrors, this.errorAction});
}

class ErrorAction {
  String name;
  String link;

  ErrorAction(this.name, this.link);
}

class FieldErrors {
  String id;
  String error;

  FieldErrors(this.id, this.error);
}