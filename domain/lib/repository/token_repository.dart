import 'package:domain/entities/token.dart';

/// An abstract repository for a Token.
abstract class TokenRepository {
  Future<Token> getRemoteToken(String username, String password);
  Future<Token> getLocalToken();
  Future<bool> hasLocalToken();
  Future<void> saveToken(Token token);
  Future<void> removeLocalToken();
}