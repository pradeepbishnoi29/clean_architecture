
//---------------------------------------------------------------------------
// Helper classes/interfaces
class MessageColumns {
  static const String messageType = "MessageType";
  static const String fourKitesLoadId = "FourKitesLoadId";
  static const String loadNumber = "LoadNumber";
  static const String proNumber = "ProNumber";
  static const String shipper = "Shipper";
  static const String tags = "Tags";
  static const String referenceNumbers = "ReferenceNumbers";
  static const String location = "Location";
  static const String city = "City";
  static const String state = "State";
  static const String country = "Country";
  static const String latitude = "Latitude";
  static const String longitude = "Longitude";
  static const String temperatureReading = "TemperatureReading";
  static const String temperatureUnit = "TemperatureUnit";
  static const String sensor = "Sensor";
  static const String odometerReading = "OdometerReading";
  static const String timestamp = "Timestamp";
  static const String timezone = "Timezone";
  static const String timezoneShortName = "TimezoneShortName";
  static const String timezoneOffset = "TimezoneOffset";
}

class TokenColumns {
  static const String token = 'token';
  static const String email = 'email';
  static const String password = 'password';
}

class ResponseColumns {
  static const String result = 'result';
  static const String fourKitesLoadId = 'FourKitesLoadId';
  static const String messageType = 'MessageType';
}
