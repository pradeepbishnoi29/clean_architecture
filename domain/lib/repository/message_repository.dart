import 'package:domain/entities/input_message_model.dart';
import 'package:domain/entities/success_response_model.dart';

/// An abstract repository for a user.
abstract class MessageRepository {
  Future<List<InputMessage>> getMessages();
  Future<SuccessResponseModel> saveMessage(InputMessage message);
}