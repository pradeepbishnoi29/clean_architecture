import 'package:domain/entities/input_message_model.dart';
import 'package:domain/repository/message_repository.dart';
import 'package:domain/entities/success_response_model.dart';

class MessageUseCase {
  MessageRepository _userRepository;

  MessageUseCase(MessageRepository userRepository) {
    _userRepository = userRepository;
  }

  Future<List<InputMessage>> getMessages() {
    return _userRepository.getMessages();
  }

  Future<SuccessResponseModel> saveMessage(InputMessage user) {
    return _userRepository.saveMessage(user);
  }
}