import 'package:domain/entities/token.dart';
import 'package:domain/repository/token_repository.dart';

class GetTokenUseCase {
  TokenRepository _repository;

  GetTokenUseCase(TokenRepository tokenRepository) {
    _repository = tokenRepository;
  }

  Future<Token> getToken(String username, String password) {
    return _repository.getRemoteToken(username, password);
  }

  Future<void> saveToken(Token token) {
    return _repository.saveToken(token);
  }

  Future<void> removeToken() {
    return _repository.removeLocalToken();
  }

  Future<bool> hasLocalToken() {
    return _repository.hasLocalToken();
  }

  Future<Token> getLocalToken() {
    return _repository.getLocalToken();
  }
}